package Mosquitto.br.com.mosquitto;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttCallBackClone implements MqttCallback {
	
	String message;
	MqttClientUI screen;
	
	public MqttCallBackClone(MqttClientUI screen) {
		// TODO Auto-generated constructor stub
		this.screen = screen;
	}
	
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub

	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {
		System.out.println("O tópico "+topic + " recebeu a mensagem: " + message.toString());
		this.message = message.toString();
		if(screen != null) {
			screen.receivedMessagesTextArea.setText(this.message.toString());
		}
		
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub

	}
	
	public String getMessage() {
		return this.message;
	}

}
