package Mosquitto.br.com.mosquitto;

import java.util.Scanner;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttCliente 
{
    public static void main( String[] args ) throws MqttException{
    	//Variáveis de controle
    			String topicDest = "";
    			String message = "";
    			String quit = "quit";
    			String test = "";
    			
    			MqttCallBackClone callback = new MqttCallBackClone(null);
    			
    			// pega a próxima linha do teclado
    			Scanner sc = new Scanner(System.in);

    			// Criação de um client mosquitto
    			System.out.println("Teste do client");

    			/*
    			 * Exemplo de uma conexão de teste MqttClient client = new
    			 * MqttClient("tcp://broker.mqttdashboard.com:1883", // URI
    			 */
    			
    			//Instancia um novo client
    			//Exemplo de URI Local: tcp://127.0.0.1:1883
    			System.out.println("Digite a URI do Broker:");
    			String uriBroker = sc.nextLine();
    			MqttClient client = new MqttClient(uriBroker, // URI
    					MqttClient.generateClientId(), // ClientId
    					new MemoryPersistence()// Persistence
    			);

    			// seta as configurações do mqtt
    			MqttConnectOptions options = new MqttConnectOptions();
    			options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
    			client.setCallback(callback);
//    			client.setCallback(new MqttCallback() {
//    				String mensagemRecebida = "";
//    				
//    				// mensagem entrante
//    				public void messageArrived(String topic, MqttMessage message) throws Exception {
//    					System.out.println("O tópico "+topic + " recebeu a mensagem: " + message);
//    					mensagemRecebida = message.toString();
//    				}
//
//    				// Chamado quando uma publicação exterior chega
//    				public void deliveryComplete(IMqttDeliveryToken token) {
//    					// TODO Auto-generated method stub
//    				}
//
//    				// chamado quando a conexão é perdida
//    				public void connectionLost(Throwable cause) {
//    					// TODO Auto-generated method stub
//    				}
//    				
//    			});

    			// Conecta o client ao server
    			client.connect();

    			// Assina o broker
    			System.out.println("insira o nome escolhido para origem");
    			String topicOri = sc.nextLine();
    			client.subscribe(topicOri, 0);

    			// testa se o client está conectado
    			System.out.println("Client está conectado?"+client.isConnected());

    			
    			// loop de conexão
    			while (!message.equals(quit) || !topicDest.equals(quit)) {

    				System.out.println("Insira o Topico para enviar a mensagem");
    				topicDest = sc.nextLine();
    				
    				System.out.println("Insira a mensagem de envio");
    				message = sc.nextLine();

    				// publica uma menssagem para o server
    				client.publish(topicDest, // topic
    						message.getBytes(), // message
    						2, // QoS
    						false); // retained?
    				
    				
    				try {
    				if (!callback.getMessage().isEmpty()) {
    					System.out.println("Conseguiu");
    				}}catch (Exception e) {
					}

    			}

    			// Fecha todas as conexões
    			sc.close();
    			client.unsubscribe(topicOri);
    			client.disconnect();
    			System.out.println("Client ainda está conectado?"+client.isConnected());
    		}
}
